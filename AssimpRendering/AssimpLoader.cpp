#include "stdafx.h"
#include "AssimpLoader.h"

#include "FreeImage.h"

#include <fstream>
#include <unordered_map>

#include <assimp/scene.h>
#include <assimp/postprocess.h>
using namespace std;
using namespace Assimp;

const float AssimpLoader::Gamma = 2.2f;

unique_ptr<Scene> AssimpLoader::loadScene(const string & directory, const string & file,
	ID3D12Device* device, ID3D12CommandQueue* pQueue)
{
	string fullPath = directory + file;

	ifstream is(fullPath);
	if (!is.is_open()) {
		throw InvalidFileException("Specified scene file not found");
	}

	Assimp::Importer importer;
	auto assimpScene = importer.ReadFile(fullPath, aiProcessPreset_TargetRealtime_Fast | aiProcess_FlipWindingOrder);
		//& ~aiProcess_SplitLargeMeshes | aiProcess_OptimizeGraph | aiProcess_OptimizeMeshes);
	if (!assimpScene) {
		throw LoadFileException("Assimp couldn't load file");
	}

	auto scene = make_unique<Scene>();

	scene->batches.resize(assimpScene->mNumMaterials);

	ComPtr<ID3D12CommandAllocator> commandAllocator;
	ComPtr<ID3D12GraphicsCommandList> commandList;

	device->CreateCommandAllocator(D3D12_COMMAND_LIST_TYPE_DIRECT, IID_PPV_ARGS(&commandAllocator));
	device->CreateCommandList(0, D3D12_COMMAND_LIST_TYPE_DIRECT, commandAllocator.Get(), nullptr, IID_PPV_ARGS(&commandList));

	CreateMeshes(assimpScene, scene.get(), device, pQueue, commandAllocator.Get(), commandList.Get());

	CreateMaterials(assimpScene, scene.get(), directory, device, pQueue, commandAllocator.Get(), commandList.Get());

	CreateLights(scene.get());

	return std::move(scene);
}

void AssimpLoader::CreateLights(Scene * resScene)
{
	resScene->light0.color = { 1.0f, 1.0f, 0.8f};
	resScene->light0.direction = { -0.1f, 1.0f, 0.05f};
}

using TextureMap = unordered_map<string, ComPtr<ID3D12Resource>>;

BYTE* LoadTextureFromFile(string file, UINT& width, UINT& height, UINT& scanWidth)
{
	FREE_IMAGE_FORMAT fmt = FIF_UNKNOWN;
	fmt = FreeImage_GetFileType(file.c_str());
	if (fmt == FIF_UNKNOWN)
		fmt = FreeImage_GetFIFFromFilename(file.c_str());

	if (fmt == FIF_UNKNOWN || !FreeImage_FIFSupportsReading(fmt)) {
		throw ResourceException(file.c_str());
	}

	FIBITMAP *img = FreeImage_Load(fmt, file.c_str(), 0);
	if (img == nullptr)
		throw ResourceException(file.c_str());

	FIBITMAP *img32 = FreeImage_ConvertTo32Bits(img);
	FreeImage_Unload(img);

	width = FreeImage_GetWidth(img32);
	height = FreeImage_GetHeight(img32);
	const UINT bpp = FreeImage_GetBPP(img32);
	scanWidth = FreeImage_GetPitch(img32);

	auto data = new BYTE[height * scanWidth];
	FreeImage_ConvertToRawBits(data, img32, scanWidth, bpp, FI_RGBA_RED_MASK,
		FI_RGBA_GREEN_MASK, FI_RGBA_BLUE_MASK, FALSE);
	return data;
}

ComPtr<ID3D12Resource> LoadTexture(string path, ID3D12Device* pDevice,
	ID3D12GraphicsCommandList* pCommandList, ID3D12Resource** ppUploadBuffer)
{
	UINT width, height, scanWidth;
	auto raw = LoadTextureFromFile(path, width, height, scanWidth);

	D3D12_RESOURCE_DESC texDesc = {};
	texDesc.Width = width;
	texDesc.Height = height;
	texDesc.DepthOrArraySize = 1;
	texDesc.MipLevels = 1;
	texDesc.SampleDesc.Count = 1;
	texDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM_SRGB;
	texDesc.Layout = D3D12_TEXTURE_LAYOUT_UNKNOWN;
	texDesc.Dimension = D3D12_RESOURCE_DIMENSION_TEXTURE2D;
	
	ComPtr<ID3D12Resource> tex;

	ThrowIfFailed(pDevice->CreateCommittedResource(
		&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD),
		D3D12_HEAP_FLAG_NONE,
		&CD3DX12_RESOURCE_DESC::Buffer(height * scanWidth),
		D3D12_RESOURCE_STATE_GENERIC_READ,
		nullptr,
		IID_PPV_ARGS(ppUploadBuffer)));

	ThrowIfFailed(pDevice->CreateCommittedResource(
		&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_DEFAULT),
		D3D12_HEAP_FLAG_NONE,
		&texDesc,
		D3D12_RESOURCE_STATE_COPY_DEST,
		nullptr,
		IID_PPV_ARGS(&tex)));
	D3D12_SUBRESOURCE_DATA textureData = {};
	textureData.pData = raw;
	textureData.RowPitch = scanWidth;
	textureData.SlicePitch = height * scanWidth;

	UpdateSubresources(pCommandList, tex.Get(), *ppUploadBuffer, 0, 0, 1, &textureData);

	delete[] raw;
	return tex;
}

ComPtr<ID3D12Resource> FindTexture(string path, TextureMap& texMaps, ID3D12Device* pDevice,
	ID3D12GraphicsCommandList* pCommandList, ID3D12Resource** ppUploadBuffer)
{
	auto texIt = texMaps.find(path);
	if (texIt != texMaps.end()) {
		return texIt->second;
	}
	else
	{
		 auto tex = LoadTexture(path, pDevice, pCommandList, ppUploadBuffer);
		 texMaps[path] = tex;
		 return tex;
	}
}

void AssimpLoader::CreateMaterials(const aiScene * aiscene, Scene * resScene, const string & directory,
	ID3D12Device* pDevice, ID3D12CommandQueue* pQueue, ID3D12CommandAllocator* pCommandAllocator,
	ID3D12GraphicsCommandList* pCommandList)
{
	ComPtr<ID3D12Resource> uploadBuffer;

	ComPtr<ID3D12Fence> fence;
	UINT fenceValue = 0;
	ThrowIfFailed(pDevice->CreateFence(fenceValue, D3D12_FENCE_FLAG_NONE, IID_PPV_ARGS(&fence)));
	++fenceValue;

	auto fenceEvent = CreateEventEx(nullptr, FALSE, FALSE, EVENT_ALL_ACCESS);
	if (fenceEvent == nullptr)
	{
		ThrowIfFailed(HRESULT_FROM_WIN32(GetLastError()));
	}

	FreeImage_Initialise();

	TextureMap texMaps;

	for (unsigned int materialId = 0; materialId < aiscene->mNumMaterials; ++materialId) {
		const auto aimat = aiscene->mMaterials[materialId];
		Material mat;

		aiString texFile;
		if (AI_SUCCESS == aimat->GetTexture(aiTextureType_DIFFUSE, 0, &texFile)) {
			string path = directory + texFile.C_Str();

			mat.diffuseTex = FindTexture(path, texMaps, pDevice, pCommandList, &uploadBuffer);
		}

		aiColor3D color;
		aimat->Get(AI_MATKEY_COLOR_AMBIENT, color);
		mat.consts.ambient = XMFLOAT3(powf(color.r, Gamma), powf(color.g, Gamma), powf(color.b, Gamma));

		aimat->Get(AI_MATKEY_COLOR_DIFFUSE, color);
		mat.consts.diffuse = XMFLOAT3(powf(color.r, Gamma), powf(color.g, Gamma), powf(color.b, Gamma));

		aimat->Get(AI_MATKEY_COLOR_SPECULAR, color);
		mat.consts.specular = XMFLOAT3(powf(color.r, Gamma), powf(color.g, Gamma), powf(color.b, Gamma));

		aimat->Get(AI_MATKEY_SHININESS, mat.consts.shininess);

		ThrowIfFailed(pCommandList->Close());
		ID3D12CommandList* ppCommandLists[] = { pCommandList };
		pQueue->ExecuteCommandLists(_countof(ppCommandLists), ppCommandLists);

		const UINT64 oldFence = fenceValue;
		ThrowIfFailed(pQueue->Signal(fence.Get(), fenceValue));
		fenceValue++;

		// Wait until the previous uploads are finished. This is not the best way to do this;
		// A ring buffer of upload buffers would probably be more efficient
		if (fence->GetCompletedValue() < oldFence)
		{
			ThrowIfFailed(fence->SetEventOnCompletion(oldFence, fenceEvent));
			WaitForSingleObject(fenceEvent, INFINITE);
		}

		ThrowIfFailed(pCommandAllocator->Reset());
		ThrowIfFailed(pCommandList->Reset(pCommandAllocator, nullptr));

		resScene->batches[materialId].material = std::move(mat);
	}

	FreeImage_DeInitialise();
	CloseHandle(fenceEvent);
}

void AssimpLoader::CreateMeshes(const aiScene * aiscene, Scene * resScene,
	ID3D12Device* pDevice, ID3D12CommandQueue* pQueue, ID3D12CommandAllocator* pCommandAllocator,
	ID3D12GraphicsCommandList* pCommandList)
{
	ComPtr<ID3D12Resource> uploadBuffers[2];

	ComPtr<ID3D12Fence> fence;
	UINT fenceValue = 0;
	ThrowIfFailed(pDevice->CreateFence(fenceValue, D3D12_FENCE_FLAG_NONE, IID_PPV_ARGS(&fence)));
	++fenceValue;

	auto fenceEvent = CreateEventEx(nullptr, FALSE, FALSE, EVENT_ALL_ACCESS);
	if (fenceEvent == nullptr)
	{
		ThrowIfFailed(HRESULT_FROM_WIN32(GetLastError()));
	}

	for (unsigned n = 0; n < aiscene->mNumMeshes; ++n) {
		Mesh mesh;
		aiMesh *aimesh = aiscene->mMeshes[n];

		{
			vector<UINT> faces(aimesh->mNumFaces * 3);
			unsigned faceIndex = 0;
			for (unsigned i = 0; i < aimesh->mNumFaces; ++i) {
				auto face = &aimesh->mFaces[i];
				memcpy(&faces[faceIndex], face->mIndices, 3 * sizeof(unsigned));
				faceIndex += 3;
			}

			UINT indicesSize = faces.size() * sizeof(UINT);

			ThrowIfFailed(pDevice->CreateCommittedResource(
				&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_DEFAULT),
				D3D12_HEAP_FLAG_NONE,
				&CD3DX12_RESOURCE_DESC::Buffer(indicesSize),
				D3D12_RESOURCE_STATE_COPY_DEST,
				nullptr,
				IID_PPV_ARGS(&mesh.indexBuffer)));

			ThrowIfFailed(pDevice->CreateCommittedResource(
				&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD),
				D3D12_HEAP_FLAG_NONE,
				&CD3DX12_RESOURCE_DESC::Buffer(indicesSize),
				D3D12_RESOURCE_STATE_GENERIC_READ,
				nullptr,
				IID_PPV_ARGS(&uploadBuffers[0])));

			D3D12_SUBRESOURCE_DATA indexData = {};
			indexData.pData = faces.data();
			indexData.RowPitch = indicesSize;
			indexData.SlicePitch = indexData.RowPitch;

			UpdateSubresources<1>(pCommandList, mesh.indexBuffer.Get(), uploadBuffers[0].Get(), 0, 0, 1, &indexData);
			pCommandList->ResourceBarrier(1, &CD3DX12_RESOURCE_BARRIER::Transition(mesh.indexBuffer.Get(), D3D12_RESOURCE_STATE_COPY_DEST, D3D12_RESOURCE_STATE_INDEX_BUFFER));

			// Describe the index buffer view.
			mesh.indexBufferView.BufferLocation = mesh.indexBuffer->GetGPUVirtualAddress();
			mesh.indexBufferView.Format = DXGI_FORMAT_R32_UINT;
			mesh.indexBufferView.SizeInBytes = indicesSize;

			mesh.numIndices = faces.size();
		}

		// vertices
		{
			UINT vertexElems = 8; // position, normal, uv
			vector<float> vertices;
			vertices.reserve(vertexElems * aimesh->mNumVertices);

			for (int n = 0; n < aimesh->mNumVertices; ++n)
			{
				vertices.push_back(aimesh->mVertices[n].x);
				vertices.push_back(aimesh->mVertices[n].y);
				vertices.push_back(aimesh->mVertices[n].z);

				vertices.push_back(aimesh->mNormals[n].x);
				vertices.push_back(aimesh->mNormals[n].y);
				vertices.push_back(aimesh->mNormals[n].z);

				vertices.push_back(aimesh->mTextureCoords[0][n].x);
				vertices.push_back(aimesh->mTextureCoords[0][n].y);
			}

			UINT verticesSize = sizeof(float) * vertices.size();

			ThrowIfFailed(pDevice->CreateCommittedResource(
				&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_DEFAULT),
				D3D12_HEAP_FLAG_NONE,
				&CD3DX12_RESOURCE_DESC::Buffer(verticesSize),
				D3D12_RESOURCE_STATE_COPY_DEST,
				nullptr,
				IID_PPV_ARGS(&mesh.vertexBuffer)));

			ThrowIfFailed(pDevice->CreateCommittedResource(
				&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD),
				D3D12_HEAP_FLAG_NONE,
				&CD3DX12_RESOURCE_DESC::Buffer(verticesSize),
				D3D12_RESOURCE_STATE_GENERIC_READ,
				nullptr,
				IID_PPV_ARGS(&uploadBuffers[1])));

			D3D12_SUBRESOURCE_DATA vertexData = {};
			vertexData.pData = vertices.data();
			vertexData.RowPitch = verticesSize;
			vertexData.SlicePitch = vertexData.RowPitch;

			UpdateSubresources<1>(pCommandList, mesh.vertexBuffer.Get(), uploadBuffers[1].Get(), 0, 0, 1, &vertexData);
			pCommandList->ResourceBarrier(1, &CD3DX12_RESOURCE_BARRIER::Transition(mesh.vertexBuffer.Get(), D3D12_RESOURCE_STATE_COPY_DEST, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER));

			// Initialize the vertex buffer view.
			mesh.vertexBufferView.BufferLocation = mesh.vertexBuffer->GetGPUVirtualAddress();
			mesh.vertexBufferView.StrideInBytes = sizeof(float) * vertexElems;
			mesh.vertexBufferView.SizeInBytes = verticesSize;
		}

		resScene->batches[aimesh->mMaterialIndex].meshes.push_back(std::move(mesh));

		ThrowIfFailed(pCommandList->Close());
		ID3D12CommandList* ppCommandLists[] = { pCommandList };
		pQueue->ExecuteCommandLists(_countof(ppCommandLists), ppCommandLists);

		const UINT64 oldFence = fenceValue;
		ThrowIfFailed(pQueue->Signal(fence.Get(), fenceValue));
		fenceValue++;

		// Wait until the previous uploads are finished.
		// See above why this is not ideal, but sufficient for the time being
		if (fence->GetCompletedValue() < oldFence)
		{
			ThrowIfFailed(fence->SetEventOnCompletion(oldFence, fenceEvent));
			WaitForSingleObject(fenceEvent, INFINITE);
		}

		ThrowIfFailed(pCommandAllocator->Reset());
		ThrowIfFailed(pCommandList->Reset(pCommandAllocator, nullptr));
	}

	CloseHandle(fenceEvent);
}
