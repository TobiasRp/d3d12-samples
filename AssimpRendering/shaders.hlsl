
cbuffer ConstantBuffer : register(b0)
{
	float4x4 mvp;
};

cbuffer MaterialConstants : register(b1)
{
	float3 ambient;
	float3 diffuse;
	float3 specular;
	float shininess;
};


interface iBaseLight
{
	float3 IlluminateDiffuse(float3 normal);
	float3 IlluminateSpecular(float3 normal, float specularPower);
};

class cDirectionalLight : iBaseLight
{
	float3 color;
	float3 direction; // light direction, NOT direction to the light

	float3 IlluminateDiffuse(float3 normal)
	{
		float dotN = clamp(dot( -direction, normal), 0.0, 1.0);
		return dotN * color;
	}

	float3 IlluminateSpecular(float3 normal, float specularPower)
	{
		return color; // TODO
	}
};

cbuffer PerFrame : register(b2)
{
	cDirectionalLight g_Light0;
}


struct PSInput
{
	float4 position : SV_POSITION;
	float3 normal : NORMAL;
	float2 uv : TEXCOORD0;
};

PSInput VSMain(float4 position : POSITION, float3 normal : NORMAL, float2 uv : TEXCOORD0)
{
	PSInput result;
	result.position = mul(position, mvp);
	result.normal = normal;
	result.uv = uv;

	return result;
}

Texture2D g_diffuseMap : register(t0);

SamplerState g_sampler : register(s0);

float4 PSMain(PSInput input) : SV_TARGET
{
	float4 diffMapColor = g_diffuseMap.Sample(g_sampler, input.uv);

	float3 lightDiffuse0 = g_Light0.IlluminateDiffuse(input.normal);

	float3 pixelColor = ambient + diffMapColor * clamp(lightDiffuse0, 0.25, 1.0);
	return float4(pixelColor, 1.0);
}
