#pragma once

using namespace DirectX;
using namespace Microsoft::WRL;

struct DirectionalLight
{
	XMFLOAT3 color;
	XMFLOAT3 direction;
};

struct Mesh
{
	ComPtr<ID3D12Resource> vertexBuffer;
	ComPtr<ID3D12Resource> indexBuffer;
	UINT numIndices;

	D3D12_VERTEX_BUFFER_VIEW vertexBufferView;
	D3D12_INDEX_BUFFER_VIEW indexBufferView;
};

struct MaterialConstants
{
	XMFLOAT3 ambient;
	XMFLOAT3 diffuse;
	XMFLOAT3 specular;
	float shininess;
};

class Material
{
public:
	MaterialConstants consts;
	ComPtr<ID3D12Resource> constantBuffer;
	ComPtr<ID3D12DescriptorHeap> srvHeap;

	ComPtr<ID3D12Resource> diffuseTex;

	void Init(ID3D12Device* pDevice);

private:
	void InitContantBuffer(ID3D12Device* pDevice);

	void InitHeaps(ID3D12Device* pDevice);
};

class Batch
{
public:
	Material material;
	vector<Mesh> meshes;

	ComPtr<ID3D12CommandAllocator> bundleAllocator;
	ComPtr<ID3D12GraphicsCommandList> bundle;

	inline void Init(ID3D12Device* pDevice, ID3D12PipelineState* pPso, ID3D12RootSignature* pRootSignature,
		ID3D12Resource* pConstantBuffer)
	{
		material.Init(pDevice);
		InitBundle(pDevice, pPso, pRootSignature, pConstantBuffer);
	}

	void PopulateCommandList(ID3D12GraphicsCommandList* commandList, ID3D12RootSignature* pRootSignature,
		ID3D12Resource* constantBuffer);

private:
	void InitBundle(ID3D12Device* pDevice, ID3D12PipelineState* pPso, ID3D12RootSignature* pRootSignature,
		ID3D12Resource* constantBuffer);
};

struct Scene
{
	vector<Batch> batches;

	DirectionalLight light0;
};
