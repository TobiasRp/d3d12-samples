#pragma once
#include "stdafx.h"
#include "SimpleCamera.h"
#include "AssimpLoader.h"

using namespace DirectX;
using namespace Microsoft::WRL;

class AssimpRendering
{
	struct Vertex
	{
		XMFLOAT3 position;
		XMFLOAT4 color;
	};

	struct ConstantBuffer
	{
		XMFLOAT4X4 MVP;
	};

	struct PerFrameBuffer
	{
		DirectionalLight light0;
	};

public:
	AssimpRendering(UINT width, UINT height);

	void Init(HWND hWnd);

	bool Event(MSG msg);

	void Update();

	void Render();

	void Destroy();

private:
	void CreateDevice();

	void CreateCommandQueue();

	void CreateSwapChain(HWND hWnd);

	void CreateDescriptorHeaps();

	void CreateFrameResources();

	void LoadPipeline(HWND hWnd);


	void CreateRootSignature();

	void CreatePipelineState();

	void InitPerFrameBuffer(const Scene* scene);

	void LoadAssets();


	void PopulateCommandList();

	void WaitForPreviousFrame();

private:
	UINT m_width, m_height;
	float m_aspectRatio;

	SimpleCamera m_camera;

	unique_ptr<Scene> m_scene;

	// Pipeline objects
	D3D12_VIEWPORT m_viewport;
	D3D12_RECT m_scissorRect;
	ComPtr<IDXGISwapChain3> m_swapChain;
	ComPtr<ID3D12Device> m_device;
	ComPtr<ID3D12Resource> m_renderTargets[2];
	ComPtr<ID3D12Resource> m_depthStencil;
	ComPtr<ID3D12CommandAllocator> m_commandAllocator;
	ComPtr<ID3D12CommandQueue> m_commandQueue;
	ComPtr<ID3D12RootSignature> m_rootSignature;
	ComPtr<ID3D12PipelineState> m_PSO;
	ComPtr<ID3D12GraphicsCommandList> m_commandList;
	ComPtr<ID3D12DescriptorHeap> m_rtvHeap;
	ComPtr<ID3D12DescriptorHeap> m_dsvHeap;
	ComPtr<ID3D12DescriptorHeap> m_samplerHeap;
	UINT m_rtvDescriptorSize;

	// Constant buffer
	ComPtr<ID3D12Resource> m_constantBuffer;
	ConstantBuffer m_constantBufferCPU;
	UINT8* m_pCbvData;

	// Per frame buffer
	ComPtr<ID3D12Resource> m_perFrameBuffer;
	PerFrameBuffer m_perFrameBufferCPU;
	UINT8* m_pPerFrameData;

	// Synchronization objects.
	UINT m_frameIndex;
	HANDLE m_fenceEvent;
	ComPtr<ID3D12Fence> m_fence;
	UINT64 m_fenceValue;
};