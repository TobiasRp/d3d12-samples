#pragma once
#include "Scene.h"
#include <assimp/Importer.hpp>

class LoadFileException : std::exception
{
public:
	LoadFileException(const char *str)
		: std::exception(str) { }
	LoadFileException() : std::exception("Could not load/parse file\n") { }
};

class InvalidFileException : std::exception
{
public:
	InvalidFileException(const char *str)
		: std::exception(str) { }
	InvalidFileException() : std::exception("Invalid file!\n") { }
};

class ResourceException : std::exception
{
public:
	ResourceException(const char *str)
		: std::exception(str) { }
	ResourceException() : std::exception("Exception while loading a resource!\n") { }
};


class AssimpLoader
{
	static const float Gamma;

public:
	AssimpLoader() = delete;
	~AssimpLoader() = delete;

	static unique_ptr<Scene> loadScene(const string& directory, const string& file,
		ID3D12Device* device, ID3D12CommandQueue* pQueue);

protected:
	static void CreateMaterials(const aiScene* aiscene, Scene* resScene, const string& directory, ID3D12Device* device,
		ID3D12CommandQueue* pQueue, ID3D12CommandAllocator* pCommandAllocator,
		ID3D12GraphicsCommandList* pCommandList);

	static void CreateMeshes(const aiScene* aiscene, Scene* resScene, ID3D12Device* device,
		ID3D12CommandQueue* pQueue, ID3D12CommandAllocator* pCommandAllocator, ID3D12GraphicsCommandList* pCommandList);

	static void CreateLights(Scene* resScene);
};

