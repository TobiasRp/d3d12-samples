#include "stdafx.h"
#include "Scene.h"


void Material::Init(ID3D12Device * pDevice)
{
	InitHeaps(pDevice);
	InitContantBuffer(pDevice);
}

void Material::InitContantBuffer(ID3D12Device* pDevice)
{
	ThrowIfFailed(pDevice->CreateCommittedResource(
		&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD),
		D3D12_HEAP_FLAG_NONE,
		&CD3DX12_RESOURCE_DESC::Buffer(sizeof(MaterialConstants)),
		D3D12_RESOURCE_STATE_GENERIC_READ,
		nullptr,
		IID_PPV_ARGS(&constantBuffer)));

	void* pConsts;
	constantBuffer->Map(0, nullptr, &pConsts);
	memcpy(pConsts, &consts, sizeof(MaterialConstants));
	constantBuffer->Unmap(0, nullptr);
}

void Material::InitHeaps(ID3D12Device * pDevice)
{
	D3D12_DESCRIPTOR_HEAP_DESC cbvSrvHeapDesc = {};
	cbvSrvHeapDesc.NumDescriptors = 1;
	cbvSrvHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;
	cbvSrvHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;
	ThrowIfFailed(pDevice->CreateDescriptorHeap(&cbvSrvHeapDesc, IID_PPV_ARGS(&srvHeap)));

	CD3DX12_CPU_DESCRIPTOR_HANDLE srvHandle(srvHeap->GetCPUDescriptorHandleForHeapStart());
	const UINT srvDescriptorSize = pDevice->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV);

	if (diffuseTex != nullptr)
	{
		D3D12_SHADER_RESOURCE_VIEW_DESC srvDesc = {};
		srvDesc.ViewDimension = D3D12_SRV_DIMENSION_TEXTURE2D;
		srvDesc.Shader4ComponentMapping = D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;
		srvDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM_SRGB;
		srvDesc.Texture2D.MipLevels = 1;
		srvDesc.Texture2D.MostDetailedMip = 0;
		srvDesc.Texture2D.ResourceMinLODClamp = 0.0f;
		pDevice->CreateShaderResourceView(diffuseTex.Get(), &srvDesc, srvHandle);

		srvHandle.Offset(srvDescriptorSize);
	}
}

void Batch::InitBundle(ID3D12Device * pDevice, ID3D12PipelineState* pPso,
	ID3D12RootSignature * pRootSignature, ID3D12Resource* constantBuffer)
{
	ThrowIfFailed(pDevice->CreateCommandAllocator(D3D12_COMMAND_LIST_TYPE_BUNDLE, IID_PPV_ARGS(&bundleAllocator)));

	ThrowIfFailed(pDevice->CreateCommandList(0,
		D3D12_COMMAND_LIST_TYPE_BUNDLE, bundleAllocator.Get(), pPso, IID_PPV_ARGS(&bundle)));

	PopulateCommandList(bundle.Get(), pRootSignature, constantBuffer);

	ThrowIfFailed(bundle->Close());
}

void Batch::PopulateCommandList(ID3D12GraphicsCommandList* pCommandList, ID3D12RootSignature* pRootSignature,
	ID3D12Resource* constantBuffer)
{
	pCommandList->SetGraphicsRootSignature(pRootSignature);
	ID3D12DescriptorHeap* ppHeaps[] = { material.srvHeap.Get() };
	pCommandList->SetDescriptorHeaps(_countof(ppHeaps), ppHeaps);

	pCommandList->SetGraphicsRootConstantBufferView(0, constantBuffer->GetGPUVirtualAddress());
	pCommandList->SetGraphicsRootConstantBufferView(1, material.constantBuffer->GetGPUVirtualAddress());
	pCommandList->SetGraphicsRootDescriptorTable(4, material.srvHeap->GetGPUDescriptorHandleForHeapStart());

	pCommandList->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	for (auto& mesh : meshes)
	{
		pCommandList->IASetIndexBuffer(&mesh.indexBufferView);
		pCommandList->IASetVertexBuffers(0, 1, &mesh.vertexBufferView);
		pCommandList->DrawIndexedInstanced(mesh.numIndices, 1, 0, 0, 0);
	}
}
