
StructuredBuffer<float> vecA : register(t0); // SRV 0
StructuredBuffer<float> vecB : register(t1); // SRV 1

RWStructuredBuffer<float> vecC : register(u0); // UAV 0

cbuffer Constants : register(b0)
{
	float factor;
	float summand;
}

[numthreads(100, 1, 1)]
void CSMain( uint3 DTid : SV_DispatchThreadID )
{
	vecC[DTid.x] = summand + factor * (vecA[DTid.x] + vecB[DTid.x]);
}