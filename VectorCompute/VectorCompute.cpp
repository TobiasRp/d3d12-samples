// VectorCompute.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

using namespace DirectX;
using namespace Microsoft::WRL;

ComPtr<ID3D12Device> g_device;

ComPtr<ID3D12CommandAllocator> g_commandAllocator;
ComPtr<ID3D12CommandQueue> g_commandQueue;
ComPtr<ID3D12GraphicsCommandList> g_commandList;

ComPtr<ID3D12Fence> g_fence;
UINT64 g_fenceValue;
HANDLE g_fenceEvent;

ComPtr<ID3D12DescriptorHeap> g_srvUavHeap;
UINT g_srvUavDescriptorSize;

ComPtr<ID3D12RootSignature> g_rootSignature;
ComPtr<ID3D12PipelineState> g_pipelineState;

ComPtr<ID3D12Resource> g_cbv;
ComPtr<ID3D12Resource> g_vecA;
ComPtr<ID3D12Resource> g_vecB;
ComPtr<ID3D12Resource> g_uploadVecA;
ComPtr<ID3D12Resource> g_uploadVecB;
ComPtr<ID3D12Resource> g_vecC;
ComPtr<ID3D12Resource> g_getVecC;

struct Constants
{
	float factor;
	float summand;
};

static const int VectorDimension = 10000;

static const float Factor = 0.5f;
static const float Summand = 0.5f;

void WaitForContext()
{
	ThrowIfFailed(g_commandQueue->Signal(g_fence.Get(), g_fenceValue));
	ThrowIfFailed(g_fence->SetEventOnCompletion(g_fenceValue, g_fenceEvent));
	++g_fenceValue;

	WaitForSingleObject(g_fenceEvent, INFINITE);
}

void Init()
{
#ifdef _DEBUG
	ComPtr<ID3D12Debug> debugCtrl;
	if (SUCCEEDED(D3D12GetDebugInterface(IID_PPV_ARGS(&debugCtrl))))
		debugCtrl->EnableDebugLayer();
#endif

	ThrowIfFailed(D3D12CreateDevice(nullptr, D3D_FEATURE_LEVEL_11_0, IID_PPV_ARGS(&g_device)));

	// Command queue, list, allocator
	D3D12_COMMAND_QUEUE_DESC comDesc;
	comDesc.Type = D3D12_COMMAND_LIST_TYPE_COMPUTE;
	comDesc.Priority = D3D12_COMMAND_QUEUE_PRIORITY_NORMAL;
	comDesc.NodeMask = 0;
	comDesc.Flags = D3D12_COMMAND_QUEUE_FLAG_DISABLE_GPU_TIMEOUT;
	
	ThrowIfFailed(g_device->CreateCommandQueue(&comDesc, IID_PPV_ARGS(&g_commandQueue)));
	ThrowIfFailed(g_device->CreateCommandAllocator(D3D12_COMMAND_LIST_TYPE_COMPUTE, IID_PPV_ARGS(&g_commandAllocator)));
	ThrowIfFailed(g_device->CreateCommandList(0, D3D12_COMMAND_LIST_TYPE_COMPUTE, g_commandAllocator.Get(), nullptr, IID_PPV_ARGS(&g_commandList)));

	// synchronization: fences and events
	ThrowIfFailed(g_device->CreateFence(g_fenceValue, D3D12_FENCE_FLAG_NONE, IID_PPV_ARGS(&g_fence)));
	++g_fenceValue;
	g_fenceEvent = CreateEventEx(nullptr, FALSE, FALSE, EVENT_ALL_ACCESS);
	if (g_fenceEvent == nullptr)
		ThrowIfFailed(HRESULT_FROM_WIN32(GetLastError()));
}

void LoadPipeline()
{
	// root signature
	{
		CD3DX12_DESCRIPTOR_RANGE ranges[2];
		ranges[0].Init(D3D12_DESCRIPTOR_RANGE_TYPE_SRV, 2, 0);
		ranges[1].Init(D3D12_DESCRIPTOR_RANGE_TYPE_UAV, 1, 0);

		CD3DX12_ROOT_PARAMETER rootParameters[3];
		rootParameters[0].InitAsConstantBufferView(0, 0, D3D12_SHADER_VISIBILITY_ALL);
		rootParameters[1].InitAsDescriptorTable(1, &ranges[0], D3D12_SHADER_VISIBILITY_ALL);
		rootParameters[2].InitAsDescriptorTable(1, &ranges[1], D3D12_SHADER_VISIBILITY_ALL);

		ComPtr<ID3DBlob> signature;
		ComPtr<ID3DBlob> error;
		CD3DX12_ROOT_SIGNATURE_DESC signatureDesc(_countof(rootParameters), rootParameters, 0, nullptr);
		ThrowIfFailed(D3D12SerializeRootSignature(&signatureDesc, D3D_ROOT_SIGNATURE_VERSION_1, &signature, &error));
		ThrowIfFailed(g_device->CreateRootSignature(0, signature->GetBufferPointer(), signature->GetBufferSize(), IID_PPV_ARGS(&g_rootSignature)));
	}
	// shader
	ComPtr<ID3DBlob> computeShader;

#ifdef _DEBUG
	// Enable better shader debugging with the graphics debugging tools.
	UINT compileFlags = D3DCOMPILE_DEBUG | D3DCOMPILE_SKIP_OPTIMIZATION;
#else
	UINT compileFlags = 0;
#endif
	
	ThrowIfFailed(D3DCompileFromFile(L"VecAddCS.hlsl", nullptr, nullptr, "CSMain", "cs_5_0", compileFlags, 0, &computeShader, nullptr));

	D3D12_COMPUTE_PIPELINE_STATE_DESC desc = {};
	desc.CS.pShaderBytecode = reinterpret_cast<UINT8*>(computeShader->GetBufferPointer());
	desc.CS.BytecodeLength = computeShader->GetBufferSize();
	desc.pRootSignature = g_rootSignature.Get();
	ThrowIfFailed(g_device->CreateComputePipelineState(&desc, IID_PPV_ARGS(&g_pipelineState)));
}

void CreateDescriptorHeaps()
{
	D3D12_DESCRIPTOR_HEAP_DESC desc = {};
	desc.NumDescriptors = 4; // cbv + 2 srv + uav
	desc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;
	desc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;
	ThrowIfFailed(g_device->CreateDescriptorHeap(&desc, IID_PPV_ARGS(&g_srvUavHeap)));

	g_srvUavDescriptorSize = g_device->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV);
}

void CreateConstantBuffer()
{
	ThrowIfFailed(g_device->CreateCommittedResource(
		&CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD),
		D3D12_HEAP_FLAG_NONE,
		&CD3DX12_RESOURCE_DESC::Buffer(sizeof(Constants)),
		D3D12_RESOURCE_STATE_GENERIC_READ,
		nullptr,
		IID_PPV_ARGS(&g_cbv)));

	Constants consts;
	consts.factor = Factor;
	consts.summand = Summand;

	Constants* pConst;
	g_cbv->Map(0, nullptr, reinterpret_cast<void**>(&pConst));
	*pConst = consts;
	g_cbv->Unmap(0, nullptr);
}

void InitializeVectorData(vector<float>& vecA, vector<float>& vecB)
{
	for (int i = 0; i < VectorDimension; ++i)
	{
		vecA.push_back(static_cast<float>(i));
		vecB.push_back(static_cast<float>(VectorDimension - i));
	}
}

void LoadResources()
{
	CreateConstantBuffer();

	vector<float> vecA, vecB;
	InitializeVectorData(vecA, vecB);
	const UINT dataSize = VectorDimension * sizeof(float);

	D3D12_HEAP_PROPERTIES defaultHeapProperties = CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_DEFAULT);
	D3D12_HEAP_PROPERTIES uploadHeapProperties = CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_UPLOAD);
	D3D12_HEAP_PROPERTIES readbackHeapProperties = CD3DX12_HEAP_PROPERTIES(D3D12_HEAP_TYPE_READBACK);
	D3D12_RESOURCE_DESC bufferDesc = CD3DX12_RESOURCE_DESC::Buffer(dataSize, D3D12_RESOURCE_FLAG_ALLOW_UNORDERED_ACCESS);

	// Use two upload heaps to move the data to the GPU, then copy them to the actual buffers
	// Maybe this is too much / not ideal for this sample...

	ThrowIfFailed(g_device->CreateCommittedResource(
		&defaultHeapProperties,
		D3D12_HEAP_FLAG_NONE,
		&bufferDesc,
		D3D12_RESOURCE_STATE_COPY_DEST,
		nullptr,
		IID_PPV_ARGS(&g_vecA)));

	ThrowIfFailed(g_device->CreateCommittedResource(
		&defaultHeapProperties,
		D3D12_HEAP_FLAG_NONE,
		&bufferDesc,
		D3D12_RESOURCE_STATE_COPY_DEST,
		nullptr,
		IID_PPV_ARGS(&g_vecB)));

	ThrowIfFailed(g_device->CreateCommittedResource(
		&uploadHeapProperties,
		D3D12_HEAP_FLAG_NONE,
		&bufferDesc,
		D3D12_RESOURCE_STATE_GENERIC_READ,
		nullptr,
		IID_PPV_ARGS(&g_uploadVecA)));

	ThrowIfFailed(g_device->CreateCommittedResource(
		&uploadHeapProperties,
		D3D12_HEAP_FLAG_NONE,
		&bufferDesc,
		D3D12_RESOURCE_STATE_GENERIC_READ,
		nullptr,
		IID_PPV_ARGS(&g_uploadVecB)));

	ThrowIfFailed(g_device->CreateCommittedResource(
		&defaultHeapProperties,
		D3D12_HEAP_FLAG_NONE,
		&bufferDesc,
		D3D12_RESOURCE_STATE_UNORDERED_ACCESS,
		nullptr,
		IID_PPV_ARGS(&g_vecC)));

	ThrowIfFailed(g_device->CreateCommittedResource(
		&readbackHeapProperties,
		D3D12_HEAP_FLAG_NONE,
		&bufferDesc,
		D3D12_RESOURCE_STATE_COPY_DEST,
		nullptr,
		IID_PPV_ARGS(&g_getVecC)));

	D3D12_SUBRESOURCE_DATA dataVecA = {};
	dataVecA.pData = reinterpret_cast<UINT8*>(&vecA[0]);
	dataVecA.RowPitch = dataSize;
	dataVecA.SlicePitch = sizeof(float);

	UpdateSubresources(g_commandList.Get(), g_vecA.Get(), g_uploadVecA.Get(), 0, 0, 1, &dataVecA);

	D3D12_SUBRESOURCE_DATA dataVecB = {};
	dataVecB.pData = reinterpret_cast<UINT8*>(&vecB[0]);
	dataVecB.RowPitch = dataSize;
	dataVecB.SlicePitch = sizeof(float);

	UpdateSubresources(g_commandList.Get(), g_vecB.Get(), g_uploadVecB.Get(), 0, 0, 1, &dataVecB);

	g_commandList->ResourceBarrier(1, &CD3DX12_RESOURCE_BARRIER::Transition(g_vecA.Get(), D3D12_RESOURCE_STATE_COPY_DEST, D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE));
	g_commandList->ResourceBarrier(1, &CD3DX12_RESOURCE_BARRIER::Transition(g_vecB.Get(), D3D12_RESOURCE_STATE_COPY_DEST, D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE));

	g_commandList->Close();
	ID3D12CommandList* ppCommandLists[] = { g_commandList.Get() };
	g_commandQueue->ExecuteCommandLists(_countof(ppCommandLists), ppCommandLists);

	// Create two srv's and the uav for the result vector
	D3D12_SHADER_RESOURCE_VIEW_DESC srvDesc = {};
	srvDesc.Shader4ComponentMapping = D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;
	srvDesc.Format = DXGI_FORMAT_UNKNOWN;
	srvDesc.ViewDimension = D3D12_SRV_DIMENSION_BUFFER;
	srvDesc.Buffer.FirstElement = 0;
	srvDesc.Buffer.NumElements = VectorDimension;
	srvDesc.Buffer.StructureByteStride = sizeof(float);
	srvDesc.Buffer.Flags = D3D12_BUFFER_SRV_FLAG_NONE;

	CD3DX12_CPU_DESCRIPTOR_HANDLE srvHandle0(g_srvUavHeap->GetCPUDescriptorHandleForHeapStart(), 1, g_srvUavDescriptorSize);
	CD3DX12_CPU_DESCRIPTOR_HANDLE srvHandle1(g_srvUavHeap->GetCPUDescriptorHandleForHeapStart(), 2, g_srvUavDescriptorSize);
	g_device->CreateShaderResourceView(g_vecA.Get(), &srvDesc, srvHandle0);
	g_device->CreateShaderResourceView(g_vecB.Get(), &srvDesc, srvHandle1);

	D3D12_UNORDERED_ACCESS_VIEW_DESC uavDesc = {};
	uavDesc.Format = DXGI_FORMAT_UNKNOWN;
	uavDesc.ViewDimension = D3D12_UAV_DIMENSION_BUFFER;
	uavDesc.Buffer.FirstElement = 0;
	uavDesc.Buffer.NumElements = VectorDimension;
	uavDesc.Buffer.StructureByteStride = sizeof(float);
	uavDesc.Buffer.CounterOffsetInBytes = 0;
	uavDesc.Buffer.Flags = D3D12_BUFFER_UAV_FLAG_NONE;

	CD3DX12_CPU_DESCRIPTOR_HANDLE uavHandle(g_srvUavHeap->GetCPUDescriptorHandleForHeapStart(), 3, g_srvUavDescriptorSize);
	g_device->CreateUnorderedAccessView(g_vecC.Get(), nullptr, &uavDesc, uavHandle);
}

vector<float> GetResultFromGPU()
{
	vector<float> res(VectorDimension);
	float* pData;
	g_getVecC->Map(0, nullptr, reinterpret_cast<void**>(&pData));

	memcpy(res.data(), pData, VectorDimension * sizeof(float));
	g_getVecC->Unmap(0, nullptr);
	return res;
}

void PopulateCommandList()
{
	ThrowIfFailed(g_commandList->Reset(g_commandAllocator.Get(), g_pipelineState.Get()));

	g_commandList->SetPipelineState(g_pipelineState.Get());
	g_commandList->SetComputeRootSignature(g_rootSignature.Get());

	ID3D12DescriptorHeap* ppHeaps[] = { g_srvUavHeap.Get() };
	g_commandList->SetDescriptorHeaps(_countof(ppHeaps), ppHeaps);

	CD3DX12_GPU_DESCRIPTOR_HANDLE srvHandle(g_srvUavHeap->GetGPUDescriptorHandleForHeapStart(), 1, g_srvUavDescriptorSize);
	CD3DX12_GPU_DESCRIPTOR_HANDLE uavHandle(g_srvUavHeap->GetGPUDescriptorHandleForHeapStart(), 3, g_srvUavDescriptorSize);

	g_commandList->SetComputeRootConstantBufferView(0, g_cbv->GetGPUVirtualAddress());
	g_commandList->SetComputeRootDescriptorTable(1, srvHandle);
	g_commandList->SetComputeRootDescriptorTable(2, uavHandle);

	g_commandList->Dispatch(VectorDimension, 1, 1);

	g_commandList->ResourceBarrier(1, &CD3DX12_RESOURCE_BARRIER::Transition(g_vecC.Get(), D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE, D3D12_RESOURCE_STATE_COPY_SOURCE));
	g_commandList->CopyResource(g_getVecC.Get(), g_vecC.Get());

	g_commandList->Close();
}

__int64 GetPerfCount()
{
	LARGE_INTEGER li;
	QueryPerformanceCounter(&li);
	return li.QuadPart;
}

double GetPerfFrequency()
{
	LARGE_INTEGER li;
	QueryPerformanceFrequency(&li);
	return li.QuadPart / 1000.0;
}

int main()
{
	Init();

	CreateDescriptorHeaps();

	LoadPipeline();

	auto counterStart = GetPerfCount();

	LoadResources();

	PopulateCommandList();
	
	ID3D12CommandList* ppCommandLists[] = { g_commandList.Get() };
	g_commandQueue->ExecuteCommandLists(_countof(ppCommandLists), ppCommandLists);

	// Wait for completion
	WaitForContext();

	auto counterEnd = GetPerfCount();
	std::cout << "Execution took " << (counterEnd - counterStart) / GetPerfFrequency() << "ms\n";

	auto res = GetResultFromGPU();

	// Verify results
	for (int i = 0; i < VectorDimension; ++i)
	{
		if (res[i] != VectorDimension * Factor + Summand)
			std::cout << "Result incorrect: " << res[i] << "\n";
	}

    return 0;
}

