# README #

This repo contains samples for using direct3d 12.

Note that I am implementing these samples while learning d3d12. Moreover, at this point there is not much available on how to best use Direct3d12, i.e. common patterns and guidelines are still missing. I have oriented myself on the official samples from MS.

## Contents ##
**VectorCompute** The classical GPGPU application that adds two vectors. This sample only uses the compute functionality and gives a concise introduction to setting up resources, descriptors, and root signatures.

**DeferredTriangle** Renders a colored triangle into a texture, which is then rendered in a separate pass to the framebuffer. This samples contains the basics for deferred shading, however, the "g-buffer" is simply a color texture and no shading is performed.

**AssimpRenderer** (*Work in progress*)Loads a scene file supported by assimp and renders static geometry with textures. Not that this project requires some work to set it up - FreeImage and assimp are needed.