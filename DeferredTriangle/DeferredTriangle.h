#pragma once
#include "stdafx.h"

using namespace DirectX;
using namespace Microsoft::WRL;


class DeferredTriangle
{
	struct Vertex
	{
		XMFLOAT3 position;
		XMFLOAT4 color;
	};

	struct QuadVertex
	{
		XMFLOAT3 position;
		XMFLOAT2 uv;
	};

public:
	DeferredTriangle(UINT width, UINT height);

	void Init(HWND hWnd);

	void Update();

	void Render();

	void Destroy();

private:
	void CreateDevice();

	void CreateCommandQueue();

	void CreateSwapChain(HWND hWnd);

	void CreateDescriptorHeaps();

	void CreateFrameResources();

	void LoadPipeline(HWND hWnd);


	void CreateRootSignature();

	void CreateGeometryState();

	void CreateShadingState();

	void CreateVertexBuffer();

	void CreateDrawTriangleBundle();

	void CreateDrawFullscreenQuadBundle();

	void LoadAssets();


	void PopulateCommandList();

	void WaitForPreviousFrame();

private:
	UINT m_width, m_height;

	// Pipeline objects
	D3D12_VIEWPORT m_viewport;
	D3D12_RECT m_scissorRect;
	ComPtr<IDXGISwapChain3> m_swapChain;
	ComPtr<ID3D12Device> m_device;
	ComPtr<ID3D12Resource> m_renderTargets[2];
	ComPtr<ID3D12CommandAllocator> m_commandAllocator;
	ComPtr<ID3D12CommandAllocator> m_bundleAllocator;
	ComPtr<ID3D12CommandQueue> m_commandQueue;
	ComPtr<ID3D12RootSignature> m_rootSignature;
	ComPtr<ID3D12PipelineState> m_geometryPSO;
	ComPtr<ID3D12PipelineState> m_shadingPSO;
	ComPtr<ID3D12GraphicsCommandList> m_commandList;
	ComPtr<ID3D12GraphicsCommandList> m_triangleBundle;
	ComPtr<ID3D12GraphicsCommandList> m_quadBundle;
	ComPtr<ID3D12DescriptorHeap> m_rtvHeap;
	ComPtr<ID3D12DescriptorHeap> m_samplerHeap;
	UINT m_rtvDescriptorSize;
	UINT m_cbvSrvUavDescriptorSize;

	// GBuffer
	static const UINT NumTargets = 1;
	ComPtr<ID3D12DescriptorHeap> m_gbufferRtvHeap;
	ComPtr<ID3D12DescriptorHeap> m_gbufferSrvHeap;
	ComPtr<ID3D12Resource> m_gbuffer[NumTargets];

	// App resources.
	ComPtr<ID3D12Resource> m_vertexBufferUpload[2];

	ComPtr<ID3D12Resource> m_triangleVertexBuffer;
	D3D12_VERTEX_BUFFER_VIEW m_triangleVertexBufferView;

	ComPtr<ID3D12Resource> m_quadVertexBuffer;
	D3D12_VERTEX_BUFFER_VIEW m_quadVertexBufferView;

	// Synchronization objects.
	UINT m_frameIndex;
	HANDLE m_fenceEvent;
	ComPtr<ID3D12Fence> m_fence;
	UINT64 m_fenceValue;
};