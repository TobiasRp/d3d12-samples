
struct PSInput
{
	float4 position : SV_POSITION;
	float2 uv : TEXCOORD0;
};

PSInput VSMain(float4 position : POSITION, float2 uv : TEXCOORD0)
{
	PSInput result;
	result.position = position;
	result.uv = uv;

	return result;
}

Texture2D g_diffuseTex : register(t0);
SamplerState g_sampler : register(s0);

float4 PSMain(PSInput input) : SV_TARGET
{
	return g_diffuseTex.Sample(g_sampler, input.uv);
	//return float4(1.0f, 1.0f, 0.0f, 1.0f);
}